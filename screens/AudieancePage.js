import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ZegoUIKitPrebuiltLiveStreaming, { AUDIENCE_DEFAULT_CONFIG } from '@zegocloud/zego-uikit-prebuilt-live-streaming-rn';
import { useRoute, useNavigation } from '@react-navigation/native';
import credentials from '../credentials';

const AudieancePage = () => {
    const route = useRoute();
    const navigation = useNavigation();
    const { userId, liveId, userName } = route.params;
    return (
        <View style={styles.container}>
            <ZegoUIKitPrebuiltLiveStreaming
                appID={credentials.AppId}
                appSign={credentials.AppSign}
                userID={userId}
                userName={userName}
                liveID={liveId}

                config={{
                    ...AUDIENCE_DEFAULT_CONFIG,
                    onLeaveLiveStreaming: () => { navigation.navigate('Home') }
                }}
            />
        </View>
    )
}

export default AudieancePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 0,
    }
})