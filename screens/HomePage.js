import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TextInput, Button } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';

const HomePage = () => {
    const navigation = useNavigation();
    const [userId, setUserId] = useState('');
    const [liveId, setLiveId] = useState('');
    const [userName, setUserName] = useState('');

    const insets = useSafeAreaInsets();

    const joinOrStart = (isHost) => {
        navigation.navigate(isHost ? 'Host' : 'Audieance', {
            userId, 
            liveId,
            userName
        })
    }
    useEffect(() => {
        setUserId(String(Math.floor(Math.random() * 1000000)))
        setLiveId(String(Math.floor(Math.random() * 10000)))
    }, [])
    return (
        <View style={[styles.container, { paddingTop: insets.top, paddingBottom: insets.bottom }]}>
            <Text style={styles.useridSty}>Your User Id: {userId}</Text>
            <Text style={[styles.liveIdSty, styles.leftPadding]}>Live Id: </Text>
            <TextInput
                placeholder='Please enter the live id ex: 2121'
                style={styles.input}
                onChangeText={(text) => setLiveId(text)}
                maxLength={4}
                value={liveId}
            />

            <Text style={[styles.liveIdSty, styles.leftPadding]}>User Name: </Text>
            <TextInput
                placeholder='Please enter the User Name ex: venky'
                style={styles.input}
                onChangeText={(text) => setUserName(text)}
                maxLength={30}
                value={userName}
            />

            <View style={[styles.buttonLine, styles.leftPadding]}>
                <Button 
                    disabled={liveId.length === 0}
                    style={styles.btn}
                    title='Start a Live'
                    onPress={() => joinOrStart(true)}
                />
                <View style={styles.btnSpacing} />
                <Button 
                    disabled={liveId.length === 0}
                    style={styles.btn}
                    title='Watch Live'
                    onPress={() => joinOrStart(false)}
                />
            </View>
        </View>
    )
}

export default HomePage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    useridSty: {
        fontSize: 14,
        color: '#2a2a2a',
        marginBottom: 27,
        paddingVertical: 12,
        paddingLeft: 20
    },
    liveIdSty: {
        fontSize: 14,
        color: '#2a2a2a',
        marginBottom: 5,
    },
    leftPadding: { 
        paddingLeft: 35
    },
    input: {
        height: 42,
        width: 305,
        borderWidth: 1,
        borderRadius: 9,
        borderColor: '#333333',
        paddingHorizontal:16,
        paddingVertical: 10,
        marginLeft: 35,
        marginBottom: 20,
        color: '#43254d'
    },
    buttonLine: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 42,
    },
    btn: {
        height: 42,
        borderRadius: 9,
        color: '#f4f7fb',
        marginLeft: 10
    },
    btnSpacing: {
        width: 13
    }

})