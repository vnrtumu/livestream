// HostPage.js
import React from 'react';
import { StyleSheet, View } from 'react-native';
import ZegoUIKitPrebuiltLiveStreaming, { HOST_DEFAULT_CONFIG } from '@zegocloud/zego-uikit-prebuilt-live-streaming-rn';
import { useRoute, useNavigation } from '@react-navigation/native';
import credentials from '../credentials';


export default function HostPage(props) {
    const route = useRoute();
    const navigation = useNavigation();
    const { userId,  liveId, userName} = route.params;
    return (
        <View style={styles.container}>
            <ZegoUIKitPrebuiltLiveStreaming
                appID={credentials.AppId}
                appSign={credentials.AppSign}
                userID={userId}
                userName={userName}
                liveID={liveId}

                config={{
                    ...HOST_DEFAULT_CONFIG,
                    onLeaveLiveStreaming: () => { navigation.navigate('Home') }
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 0,
    }
});