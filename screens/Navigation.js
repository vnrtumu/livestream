// In App.js in a new project

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomePage from './HomePage';
import HostPage from './HostPage';
import AudieancePage from './AudieancePage';

const Stack = createNativeStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={HomePage} />
        <Stack.Screen name="Host" component={HostPage} />
        <Stack.Screen name="Audieance" component={AudieancePage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;